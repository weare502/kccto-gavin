<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-text">
			<div class="row">
				<div class="col-10">
					<h3>KCCTO</h3>
				</div>
				<div class="col-2 social-icons">
					<img src="<?=get_stylesheet_directory_uri()?>/assets/images/Facebook.png" alt="Facebook" />
					<img src="<?=get_stylesheet_directory_uri()?>/assets/images/Pinterest.png" alt="Pinterest" />
					<img src="<?=get_stylesheet_directory_uri()?>/assets/images/Twitter.png" alt="Twitter" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<p>2323 Anderson Avenue</p>
					<p>Suite 151</p>
					<p><br /></p>
					<p>Manhattan, Kansas</p>
					<p>66502</p>
				</div><!-- /col -->
				<div class="col-md-3">
					<p>(800) 227-3578</p>
					<p>(785) 532-7197</p>
					<p><br /></p>
					<p>kccto.inc@gmail.com</p>
				</div>
			</div><!-- /row -->
			<div class="row mt-4">
				<div class="col-md-6">
					Copyright &copy; 2019-2020  - Kansas Child Care Training Opportunities - All rights reserved.
				</div>
				<div class="col-md-3">
					Website Policies &amp; Disclosures
				</div>
				<div class="col-md-3">
					Design & Development by 502
				</div>
			</div>
		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
