<?php
/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );
/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}
/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */


/**
 * Disable Gutenberg
 *
 */
add_filter('use_block_editor_for_post', '__return_false', 10);


/**
 * Add Bootstrap styles
 *
 */
function sf_child_theme_enqueue_styles() {
  wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );
}
add_action( 'wp_enqueue_scripts', 'sf_child_theme_enqueue_styles');

/**
 * Add Bootstrap scripts
 *
 */
function sf_child_theme_enqueue_scripts() {
    wp_enqueue_script( 'jqslim', '//code.jquery.com/jquery-3.2.1.slim.min.js' );
    wp_enqueue_script( 'popper', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js' );
    wp_enqueue_script( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' );
}
add_action( 'wp_enqueue_scripts', 'sf_child_theme_enqueue_scripts');

/**
 * Add meta boxes in the editor
 *
 */
function sf_child_theme_add_meta_boxes() {
  global $post;

  // Add meta boxes for the front page
  if ($post->ID == get_option( 'page_on_front' )) {
    // Remove the WP editor from the front page - we don't need it
    remove_post_type_support('page', 'editor');

    // Opening title
    add_meta_box(
        'opening_title_meta', // $id
        'Opening Title', // $title
        'sf_child_theme_opening_title_callback', // $callback
        'page', // $page
        'normal', // $context
        'high'); // $priority

    // Opening text
    add_meta_box(
        'opening_text_meta', // $id
        'Opening Text', // $title
        'sf_child_theme_opening_text_callback', // $callback
        'page', // $page
        'normal', // $context
        'high'); // $priority

    // Opening Learn More boxes
    add_meta_box(
        'opening_learn_meta', // $id
        'Opening Learn More boxes', // $title
        'sf_child_theme_opening_learn_callback', // $callback
        'page', // $page
        'normal', // $context
        'high'); // $priority

      // Opening Learn More boxes
      add_meta_box(
          'full_learn_meta', // $id
          'Full Learn More boxes', // $title
          'sf_child_theme_full_learn_callback', // $callback
          'page', // $page
          'normal', // $context
          'high'); // $priority
  }
}
add_action( 'add_meta_boxes_page', 'sf_child_theme_add_meta_boxes' );


/**
 * HTML for the Opening Title meta box
 *
 */
function sf_child_theme_opening_title_callback($post) {
  ?>
  <label for="opening_title_text">Set the text for the opening title</label>
  <input id="opening_title_text" name="opening_title_text" type="text" style="width: 100%" value="<?=get_post_meta($post->ID, '_opening_title_meta_key', true)?>" />
  <?php
}


/**
 * HTML for the Opening Text meta box
 *
 */
function sf_child_theme_opening_text_callback($post) {
  ?>
  <label for="opening_text">Set the opening text</label>
  <?php
  // Existing opening text value
  $text = get_post_meta($post->ID, '_opening_text_meta_key', true);
  // WP rich text editor
  wp_editor( htmlspecialchars_decode($text), 'opening_text', $settings = array('textarea_name' => 'opening_text') );
}


/**
 * HTML for the Opening Learn More meta box
 *
 */
function sf_child_theme_opening_learn_callback($post) {
  ?>
  <label for="opening_learn">Choose the posts to use in the opening Learn More boxes. These are existing WordPress posts - an excerpt from the post will be used on the homepage</label>
  <p>
    <select name="opening_learn1">
    <?php
    // Go through all of the posts and list them
    foreach(get_posts() as $p):
    ?>
      <option value="<?=$p->ID?>" <?=selected(get_post_meta($post->ID, '_opening_learn1_meta_key', true), $p->ID)?>><?=$p->post_title?></option>
    <?php
    endforeach;
    ?>
    </select>

    <select name="opening_learn2">
    <?php
    // Go through all of the posts and list them
    foreach(get_posts() as $p):
    ?>
      <option value="<?=$p->ID?>" <?=selected(get_post_meta($post->ID, '_opening_learn2_meta_key', true), $p->ID)?>><?=$p->post_title?></option>
    <?php
    endforeach;
    ?>
    </select>

    <select name="opening_learn3">
    <?php
    // Go through all of the posts and list them
    foreach(get_posts() as $p):
    ?>
      <option value="<?=$p->ID?>" <?=selected(get_post_meta($post->ID, '_opening_learn3_meta_key', true), $p->ID)?>><?=$p->post_title?></option>
    <?php
    endforeach;
    ?>
    </select>
  </p>

  <?php

}


/**
 * HTML for the Full Learn More meta box
 *
 */
function sf_child_theme_full_learn_callback($post) {
  ?>
  <label for="full_learn">Choose the posts to use in the full size Learn More boxes. These are existing WordPress posts - an excerpt from the post will be used on the homepage</label>
  <p>
    <select name="full_learn1">
    <?php
    // Go through all of the posts and list them
    foreach(get_posts() as $p):
    ?>
      <option value="<?=$p->ID?>" <?=selected(get_post_meta($post->ID, '_full_learn1_meta_key', true), $p->ID)?>><?=$p->post_title?></option>
    <?php
    endforeach;
    ?>
    </select>
  </p>
  <p>
    <select name="full_learn2">
    <?php
    // Go through all of the posts and list them
    foreach(get_posts() as $p):
    ?>
      <option value="<?=$p->ID?>" <?=selected(get_post_meta($post->ID, '_full_learn2_meta_key', true), $p->ID)?>><?=$p->post_title?></option>
    <?php
    endforeach;
    ?>
    </select>
  </p>

  <?php

}


/**
 * Save the meta box data
 *
 */
function sf_child_theme_save_postdata($post_id)
{
  // Opening Title meta box
  if (array_key_exists('opening_title_text', $_POST))
    update_post_meta(
        $post_id,
        '_opening_title_meta_key',
        $_POST['opening_title_text']
    );

  // Opening Title meta box
  if (array_key_exists('opening_text', $_POST))
    update_post_meta(
        $post_id,
        '_opening_text_meta_key',
        $_POST['opening_text']
    );

  // Opening Learn More meta box 1
  if (array_key_exists('opening_learn1', $_POST))
    update_post_meta(
        $post_id,
        '_opening_learn1_meta_key',
        $_POST['opening_learn1']
    );

  // Opening Learn More meta box 2
  if (array_key_exists('opening_learn2', $_POST))
    update_post_meta(
        $post_id,
        '_opening_learn2_meta_key',
        $_POST['opening_learn2']
    );

  // Opening Learn More meta box 3
  if (array_key_exists('opening_learn3', $_POST))
    update_post_meta(
        $post_id,
        '_opening_learn3_meta_key',
        $_POST['opening_learn3']
    );

  // Full Learn More meta box 1
  if (array_key_exists('full_learn1', $_POST))
    update_post_meta(
        $post_id,
        '_full_learn1_meta_key',
        $_POST['full_learn1']
    );

  // Full Learn More meta box 2
  if (array_key_exists('full_learn2', $_POST))
    update_post_meta(
        $post_id,
        '_full_learn2_meta_key',
        $_POST['full_learn2']
    );

}
add_action('save_post', 'sf_child_theme_save_postdata');
