<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Full width
 *
 * @package storefront
 */

get_header(); ?>
	<div id="primary" class="main-content">
		<main id="main" class="site-main" role="main">
			<section id="opening" class="col-md-6 col-12">
				<h1 class="opening-title">
					<?=get_post_meta($post->ID, '_opening_title_meta_key', true)?>
				</h1>
				<div class="opening-text">
					<?=get_post_meta($post->ID, '_opening_text_meta_key', true)?>
				</div>
				<div class="opening-link">
					<a href="#"><button class="opening-link-btn">Training Options & Registration</button></a>
				</div>
			</section>

			<section id="opening-learn-more">
				<div class="opening-learn-more-container">
					<div class="opening-learn-more-col col-md-4 col-12">
						<?php
						// Get post data for the first box
						$boxid = get_post_meta($post->ID, '_opening_learn1_meta_key', true);
						$learn1 = get_post($boxid);
						?>
						<h2><?=$learn1->post_title?></h2>
						<p><?=empty($learn1->post_excerpt) ? wp_trim_words($learn1->post_content, 55, '...') : $learn1->post_excerpt?></p>
						<p class="opening-learn-more-link"><a href="<?=get_permalink($boxid)?>">Learn more...</a></p>
					</div>

					<div class="opening-learn-more-col col-md-4 col-12">
						<?php
						// Get post data for the second box
						$boxid = get_post_meta($post->ID, '_opening_learn2_meta_key', true);
						$learn2 = get_post($boxid);
						?>
						<h2><?=$learn2->post_title?></h2>
						<p><?=empty($learn2->post_excerpt) ? wp_trim_words($learn2->post_content, 55, '...') : $learn2->post_excerpt?></p>
						<p class="opening-learn-more-link"><a href="<?=get_permalink($boxid)?>">Learn more...</a></p>
					</div>

					<div class="opening-learn-more-col col-md-4 col-12">
						<?php
						// Get post data for the third box
						$boxid = get_post_meta($post->ID, '_opening_learn3_meta_key', true);
						$learn3 = get_post($boxid);
						?>
						<h2><?=$learn3->post_title?></h2>
						<p><?=empty($learn3->post_excerpt) ? wp_trim_words($learn3->post_content, 55, '...') : $learn3->post_excerpt?></p>
						<p class="opening-learn-more-link"><a href="<?=get_permalink($boxid)?>">Learn more...</a></p>
					</div>
				</div><!-- /opening-learn-more-container -->
			</section>

			<section id="primary-full-learn-more" class="full-learn-more">
				<div class="full-learn-more-container">
					<?php
					// Get post data for the first full box
					$boxid = get_post_meta($post->ID, '_full_learn1_meta_key', true);
					$flearn1 = get_post($boxid);
					?>
					<div class="<?=(get_the_post_thumbnail_url($boxid) == '' ? 'col-md-12' : 'col-md-7')?> col-12" style="float: left;">
						<h2><?=$flearn1->post_title?></h2>
						<p><?=empty($flearn1->post_excerpt) ? wp_trim_words($flearn1->post_content, 55, '...') : $flearn1->post_excerpt?></p>
						<p>
							<a href="<?=get_permalink($boxid)?>"><button class="full-learn-more-btn">Learn more</button></a>
						</p>
					</div>
					<?php if (get_the_post_thumbnail_url($boxid) != ''): ?>
					<div class="col-md-5 d-none d-md-block" style="float: left;">
						<img src="<?=get_the_post_thumbnail_url($boxid)?>" alt="<?=$flearn1->post_title?>" />
					</div>
					<?php endif; ?>
				</div>
			</section>

			<section id="secondary-full-learn-more" class="full-learn-more">
				<div class="full-learn-more-container">
					<div class="col-12">
						<?php
						// Get post data for the second full box
						$boxid = get_post_meta($post->ID, '_full_learn2_meta_key', true);
						$flearn2 = get_post($boxid);
						?>
						<h2><?=$flearn2->post_title?></h2>
						<p><?=empty($flearn2->post_excerpt) ? wp_trim_words($flearn2->post_content, 55, '...') : $flearn2->post_excerpt?></p>
						<p>
							<a href="<?=get_permalink($boxid)?>"><button class="full-learn-more-btn">Learn more</button></a>
						</p>
					</div>
				</div>
			</section>

			<section id="homepage-tab-section">
				<nav>
				  <div class="nav nav-tabs" id="nav-tab" role="tablist">
				    <a class="nav-item nav-link active col-4 text-center" id="nav-providers-tab" data-toggle="tab" href="#nav-providers" role="tab" aria-controls="nav-providers" aria-selected="true">Providers</a>
				    <a class="nav-item nav-link col-4 text-center" id="nav-resources-tab" data-toggle="tab" href="#nav-resources" role="tab" aria-controls="nav-resources" aria-selected="false">Resources</a>
				    <a class="nav-item nav-link col-4 text-center" id="nav-trainers-tab" data-toggle="tab" href="#nav-trainers" role="tab" aria-controls="nav-trainers" aria-selected="false">Trainers</a>
				  </div>
				</nav>
				<div class="tab-content" id="nav-tabContent">
					<!-- Providers Tab -->
				  <div class="tab-pane fade show active" id="nav-providers" role="tabpanel" aria-labelledby="nav-providers-tab">
						<div class="row">
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="New Providers" />
								New Providers
							</div>
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="ITSN" />
								ITSN
							</div>
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="Communities of Practice" />
								Communities of Practice
							</div>
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="Individualized Professional Development Plans (IPDP)" />
								Individualized Professional Development Plans (IPDP)
							</div>
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="CDA" />
								CDA
							</div>
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="Directors" />
								Directors
							</div>
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="Scholarships" />
								Scholarships
							</div>
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="Technical Assistance" />
								Technical Assistance
							</div>
							<div class="col-md-4 col-6 text-center">
								<img src="<?=get_stylesheet_directory_uri()?>/assets/images/talking.png" alt="In-Person Training Requests" />
								In-Person Training Requests
							</div>
						</div><!-- /row -->
					</div>
					<!-- /Providers Tab -->

				  <div class="tab-pane fade" id="nav-resources" role="tabpanel" aria-labelledby="nav-resources-tab">Resources</div>

				  <div class="tab-pane fade" id="nav-trainers" role="tabpanel" aria-labelledby="nav-trainers-tab">Trainers</div>
				</div>
			</section>



			<?php
			// while ( have_posts() ) :
			// 	the_post();
			//
			// 	do_action( 'storefront_page_before' );
			//
			// 	get_template_part( 'content', 'page' );
			//
			// 	/**
			// 	 * Functions hooked in to storefront_page_after action
			// 	 *
			// 	 * @hooked storefront_display_comments - 10
			// 	 */
			// 	do_action( 'storefront_page_after' );
			//
			// endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
