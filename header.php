<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">
	<?php do_action( 'storefront_before_header' ); ?>

	<header id="masthead" class="site-header" role="banner" style="">
		<div class="header-wrapper">
			<div class="cart-links">
				<button class="my-courses">My Courses</button>
				<button class="shopping-cart"><i class="fa fa-shopping-cart"> </i></button>
			</div>

			<div class="header-row">
				<div class="site-branding">
					<div class="header-logo"><img src="<?=get_stylesheet_directory_uri()?>/assets/images/logo.png" alt="KCCTO" /></div>
					<!-- <div class="beta site-title"><a href="/" rel="home">KCCTO</a></div> -->
				</div>
				<div class="storefront-primary-navigation">
					<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'storefront' ); ?>">
					<button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></button>
						<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => 'primary-navigation',
							)
						);

						wp_nav_menu(
							array(
								'theme_location'  => 'handheld',
								'container_class' => 'handheld-navigation',
							)
						);
						?>
					</nav><!-- #site-navigation -->
				</div><!-- /storefront-primary-navigation -->
			</div><!-- /header-row -->
		</div><!-- /full-col -->
	</header>
	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
	do_action( 'storefront_before_content' );
	?>

	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		<?php
		do_action( 'storefront_content_top' );
